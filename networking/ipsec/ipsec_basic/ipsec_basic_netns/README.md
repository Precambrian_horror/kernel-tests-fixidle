# networking/ipsec/ipsec_basic/ipsec_basic_netns
This suite provides ipsec compression testing.

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
